<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 19/07/2018
 * Time: 14:00
 */

namespace App\Http\Controllers\MyAuth;


use App\Permission;
use App\Role;

use App\User;
use App\Http\Controllers\Controller;

class TempController extends Controller
{

    public function create_test_user()
    {

        $last_created_user = User::orderBy('id', 'desc')->first();
        $id = $last_created_user->id + 1;

        $user = new User(['name' => 'Testor' . ($id), 'email' => "testor{$id}@gmail.com", 'password' => 'secret']);
        $user->save();

        return redirect('/');
    }

    public function update_admin()
    {
        $user_admin = User::find(2);
        $user_admin->password = bcrypt('secret');
        $user_admin->save();

        return redirect('/');
    }

    public function set_roles_to_user()
    {
        $user = User::where('email', '=', 'admin@test.com')->first();
        $second_user = User::where('email', '=', 'second@test.com')->first();
        if (is_null($second_user)) {
            $second_user = User::create(['name' => 'second', 'email' => 'second@test.com', 'password' => \Hash::make('secret')]);
        }


        $role = Role::where('name', '=', 'admin')->first();
        $user->attachRole($role);
        $second_user->attachRole($role);

        $role = Role::where('name', '=', 'superAdmin')->first();
        $user->attachRole($role);

        return redirect('/');
    }

    public function create_entrust_needs()
    {
        // roles = user, admin, superAdmin
        \DB::statement('TRUNCATE roles CASCADE');
        \DB::statement('TRUNCATE permissions CASCADE');

        $user = new \App\Role();
        $user->name = 'user';
        $user->display_name = 'common user'; // optional
        $user->description = 'User with the common permissions'; // optional
        $user->save();

        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description = 'User is allowed to show  users and their todolists and tasks'; // optional
        $admin->save();

        $superAdmin = new Role();
        $superAdmin->name = 'superAdmin';
        $superAdmin->display_name = 'User Super Administrator'; // optional
        $superAdmin->description = 'User is allowed to manage and edit other users and their todolists and tasks count'; // optional
        $superAdmin->save();


//        $user = User::where('name', '=', 'admin')->first();

        // role attach alias
//        $user->attachRole($admin); // parameter can be an Role object, array, or id

        // or eloquent's original technique
//        $user->roles()->attach($admin->id); // id only

        // permissions - User: createTodolist
        /*
         * Permissions
         *
         * User:
         *  createTodolist
         *  createTask
         *
         * Admin:
         *  showUsers
         *
         * SuperAdmin:
         *  changeMaxTasks
         */

//        \DB::table('roles')->truncate();
//        DB::statement('TRUNCATE roles CASCADE');


        $createTodolist = new Permission();
        $createTodolist->name = 'create-todolist';
        $createTodolist->display_name = 'Create todo list'; // optional
        $createTodolist->description = 'create new todo list with default value 5'; // optional
        $createTodolist->save();

        $createTask = new Permission();
        $createTask->name = 'create-task';
        $createTask->display_name = 'Create task for todo'; // optional
        $createTask->description = 'create task to todo with done false'; // optional
        $createTask->save();

        $showUsers = new Permission();
        $showUsers->name = 'show-users';
        $showUsers->display_name = 'Create task for todo'; // optional
        $showUsers->description = 'create task to todo with done false'; // optional
        $showUsers->save();


        $changeMaxTasks = new Permission();
        $changeMaxTasks->name = 'change-maxtasks';

        $user->attachPermission($createTodolist);
        $user->attachPermission($createTask);
        // equivalent to $admin->perms()->sync(array($createPost->id));

        $admin->attachPermission($showUsers);

        $superAdmin->attachPermission($showUsers);

//        $superAdmin->attachPermissions(array($createPost, $editUser));
        // equivalent to $owner->perms()->sync(array($createPost->id, $editUser->id));


        return redirect('/');
    }


}