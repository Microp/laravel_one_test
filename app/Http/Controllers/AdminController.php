<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 24/07/2018
 * Time: 10:30
 */

namespace App\Http\Controllers;


use App\Task;
use App\Todolist;
use Illuminate\Support\Facades\Input;

class AdminController
{

    public function table_todolists()
    {
        $todolists = Todolist::all();

        return view('admin/table_todolists', ['todolists' => $todolists]);
//        return $todolists;
    }

    public function table_todolists_by_one($id)
    {
        $todolists = Todolist::all();

        $todolist_shown = Todolist::findOrFail($id);
        $tasks = Task::where('todolist_id', '=', "$todolist_shown->id")->get();

        return view("admin/table_todolists", [
            'todolists' => $todolists,
            'todolist_shown' => $todolist_shown,
            'tasks' => $tasks
        ]);
    }

    public function change_max_tasks(\Request $request, $todolist_id)
    {
//        if (!\Auth::user()->hasRole('superAdmin')) {
//            return redirect()->back()->with('message', 'Method change max tasks for todolist is not allowed for you');
//        }
        $todolist = Todolist::findOrFail($todolist_id);
        $tasks_count_now = Task::where('todolist_id', '=', "$todolist_id")->count();

        $tasks_count_new = Input::get('tasks_max_count');

        if ($tasks_count_new < $tasks_count_now) {
            return view('home', [
                'messages' => "You can not reduce the value lower than there is now"
            ]);
        }

        $todolist->max_tasks = $tasks_count_new;
        $todolist->save();


        return $this->table_todolists_by_one($todolist_id);
    }

}