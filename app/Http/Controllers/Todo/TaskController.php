<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Task;
use App\Todolist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Todolist $todolist)
    {
        return Task::where('todolist_id', '=', "$todolist->id")->get();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Todolist $todolist)
    {
        $task_count = Task::where('todolist_id', '=', "$todolist->id")->count();

//        return $task_count;
        if ($task_count >= $todolist->max_tasks) {
            return response()->json("You cannot add new tasks to list cause it exceeds limit that equal to $todolist->max_tasks", 403);
        }

        $task = Task::create([
            'action_name' => $request->get('action_name'),
            'todolist_id' => $todolist->id
        ]);
        return response()->json($task, 201);
    }

    public function change_done(Request $request, Todolist $todolist, Task $task)
    {
        $task->done = (!$task->done);
        $task->save();
        return response()->json($task, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function show(Todolist $todolist, Task $task)
    {
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todolist $todolist, Task $task)
    {
        $task->update($request->all());
        return response()->json($task, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function delete(Todolist $todolist, Task $task)
    {
        $task->delete();
        return response()->json(null, 204);
    }
}
