<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 19/07/2018
 * Time: 11:41
 */

namespace App\Http\Controllers\Todo;


use App\Task;
use App\Todolist;
use App\Http\Controllers\Controller;
use App\User;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use Zizaco\Entrust\Entrust;

//use Zizaco\Entrust\Entrust;

class TodolistController extends Controller
{

    public function index()
    {
        return Todolist::all();
    }

    public function all_for_user($user_id)
    {
        return Todolist::where('user_id', '=', "$user_id")->get();
    }

    public function show(Todolist $todolist)
    {
//        if (Entrust::hasRole('user')) {
        if (\Auth::user()->hasRole('admin')) {
            return $todolist;
        }
        return response()->json('Forbidden, you do not have a role user', 404);
    }

    public function store(Request $request)
    {
        $todolist = Todolist::create($request->all());
        return response()->json($todolist, 201);
    }

    public function update(Request $request, Todolist $todolist)
    {

//        return response()->json($request->all(), 200);
        $todolist->update($request->all());

//        return re
        return response()->json($todolist, 200);
    }

    public function delete(Todolist $todolist)
    {
        $todolist->delete();
        return response()->json(null, 204);
    }

}