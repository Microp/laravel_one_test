<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check() || !Auth::user()->hasRole('admin')) {
//        if (!\Auth::user()->hasRole('admin')) {
//            return response('Forbidden', 403);
            return redirect()->back()->with('message', 'page not allowed');
//            return view('welcome'); //, ['message' => 'You do not have permissions of role admin']);
        }
        return $next($request);
    }
}
