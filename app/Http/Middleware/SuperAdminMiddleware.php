<?php
/**
 * Created by PhpStorm.
 * User: avn
 * Date: 26/07/2018
 * Time: 17:11
 */

namespace App\Http\Middleware;


use Auth;
use Closure;

class SuperAdminMiddleware
{

    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole('superAdmin')) {
            return redirect()->back()->with('message', 'Page is not allowed for you');

        }
        return $next($request);
    }

}