<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'action_name',
        'todolist_id',
    ];

    public function todolist()
    {
        return $this->belongsTo('\App\Todolist');
    }
}
