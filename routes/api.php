<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Todolist;

Route::group(['middleware' => 'auth:api'], function () {
// Todo_list api
    Route::get('todolists', 'Todo\TodolistController@index');
    Route::get('todolists/{todolist}', 'Todo\TodolistController@show');
    Route::post('todolists', 'Todo\TodolistController@store');
    Route::put('todolists/{todolist}', 'Todo\TodolistController@update');
    Route::delete('todolists/{todolist}', 'Todo\TodolistController@delete');

    Route::get('todolists_for_user/{user_id}', 'Todo\TodolistController@all_for_user');

// Task api
    Route::get('todolists/{todolist}/tasks', 'Todo\TaskController@index');
    Route::get('todolists/{todolist}/tasks/{task}', 'Todo\TaskController@show');
    Route::post('todolists/{todolist}/tasks', 'Todo\TaskController@store');
    Route::put('todolists/{todolist}/tasks/{task}', 'Todo\TaskController@update');
    Route::put('todolists/{todolist}/tasks/{task}/change_done', 'Todo\TaskController@change_done');
    Route::delete('todolists/{todolist}/tasks/{task}', 'Todo\TaskController@delete');
});


Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => 'client-id',
        'redirect_uri' => 'http://localhost/auth/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://todo.test/oauth/authorize?'.$query);
});