<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('tables/todolists', 'AdminController@table_todolists');
    Route::get('tables/todolists/{id}', 'AdminController@table_todolists_by_one');
    Route::post('tables/todolists/{id}/change_max_tasks', 'AdminController@change_max_tasks')
        ->middleware(['superAdmin']);
});

Route::get('/temp_test', function () {
    return view('temp');
});


Route::get('/entrust/needs', 'MyAuth\TempController@create_entrust_needs');
Route::get('/entrust/set_admin_role', 'MyAuth\TempController@set_roles_to_user');

Route::get('/home', 'HomeController@index')->name('home');
