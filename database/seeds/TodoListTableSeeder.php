<?php

use Illuminate\Database\Seeder;
use App\Todolist;

class TodoListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('todolists')->truncate();
        \DB::statement('TRUNCATE todolists CASCADE');
        $faker = \Faker\Factory::create();
        $name_choice = [
            'job',
            'books',
            'home',
            'purchases'
        ];


        $min_user_id = \App\User::all()->sortBy('id')->first()->id;
        $max_user_id = \App\User::all()->sortByDesc('id')->first()->id;


        for ($i = 0; $i < 20; $i++) {
            Todolist::create([
                'name' => $name_choice[rand(0, 3)],
                'user_id' => rand($min_user_id, $max_user_id),
            ]);
        }


    }
}
