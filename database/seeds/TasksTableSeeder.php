<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tasks')->truncate();

        $faker = Faker\Factory::create();
        $min_todolist_id = \App\Todolist::all()->sortBy('id')->first()->id;
        $max_todolist_id = \App\Todolist::all()->sortByDesc('id')->first()->id;


        $action_choices = [
            'buy a car',
        ];

        for ($i = 0; $i < 50; $i++) {
            $todolist_id = rand($min_todolist_id, $max_todolist_id);
            $todolist = \App\Todolist::findOrFail($todolist_id);
//            $this->command->info(\App\Task::where('todolist_id', '=', "$todolist_id")->count());
            if ($todolist->max_tasks > \App\Task::where('todolist_id', '=', "$todolist_id")->count()) {
                $task = new \App\Task([
                        'action_name' => $faker->sentence('2'),
                        'todolist_id' => $todolist_id
                    ]
                );
                $task->save();
            }
        }
    }
}
