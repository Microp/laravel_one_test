<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();

        $tables = [
            'users',
            'todolists',
            'tasks',
        ];


        $this->command->info('Truncating existing tables');
        DB::statement('TRUNCATE TABLE ' . implode(',', $tables) . ' CASCADE;');

        $this->call(UsersTableSeeder::class);
        $this->call(TodoListTableSeeder::class);
        $this->call(TasksTableSeeder::class);


        \Illuminate\Database\Eloquent\Model::reguard();
    }
}
