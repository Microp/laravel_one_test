@extends('adminlte::page')

@section('content')


    @if (session('message'))
        <div class="alert alert-warning">
            {{ session('message') }}
        </div>
    @endif

    @if(!isset($todolist_shown))
    <p>Table todolists</p>
    <table id="table_todolists" class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="tableheader">
            <th style="width:40px">#</th>
            <th style="width:140px">Name</th>
            <th style="width:140px">User_id</th>
            <th style="width:140px">Max tasks</th>
        </tr>
        </thead>
        <tbody>
        @foreach($todolists as $todolist)
            <tr>
                <th><a href="/admin/tables/todolists/{{$todolist->id}}">{{ $todolist->id }}</a></th>
                <th>{{ $todolist->name }}</th>
                <th>{{ $todolist->user_id }}</th>
                <th>{{ $todolist->max_tasks }}</th>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

    @if(isset($todolist_shown))

        <form action="/admin/tables/todolists/{{ $todolist_shown->id }}/change_max_tasks" method="post" class="">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="">Set max tasks</label>
                <input type="text" name="tasks_max_count" value="{{ $todolist_shown->max_tasks }}">
                <input type="submit" value="Submit">
            </div>
        </form>

        <table class="table">
            <thead>
            <tr class="tableheader">
                <th style="width:40px">#</th>
                <th style="width:140px">Name</th>
                <th style="width:140px">User_id</th>
                <th style="width:140px">Max tasks</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>{{ $todolist_shown->id }}</th>
                <th>{{ $todolist_shown->name }}</th>
                <th>{{ $todolist_shown->user_id }}</th>
                <th>{{ $todolist_shown->max_tasks }}</th>
            </tr>
            </tbody>
        </table>

        <table id="table_tasks" class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="tableheader">
                <td>Action name</td>
                <td>Done</td>
            </tr>
            </thead>

            <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->action_name }}</td>
                    <td>{{ ($task->done ? '✅' : '⚿') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>


    @endif
@stop


@section('js')
    <script !src="">
        $(function () {
            $("#table_todolists, #table_tasks").DataTable();
        });
    </script>

@stop
