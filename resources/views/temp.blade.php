<?php

use App\Todolist;

?>

@extends('welcome')
@section('content')

    <h1>temprorary page</h1>
    <hr>
    @foreach(\App\Todolist::all() as $todo_list)
        <p><b>{{ $todo_list->id }}</b>: {{ $todo_list->name }}</p>
    @endforeach
    <hr>
    List of users
    <hr>
    <div class="flex-center">

        <table border="1px">
            @foreach(\App\User::all() as $user)
                <tr>
                    <td>

                        <p><b>{{ $user->id }}</b>: {{ $user->name }}</p>
                    </td>
                    <td>
                        @if(!($user->todo_lists->isEmpty()))
                            <p><b>Todo lists of user {{ $user->name }}</b></p>

                            @foreach($user->todo_lists as $todo_list)
                                <p><b>{{ $todo_list->id }}</b>: {{ $todo_list->name }}</p>
                            @endforeach

                        @else
                            <p><b>User</b> doesn't have a todo lists</p>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>

    </div>
    <a href="/">Back</a>
@endsection('content')
